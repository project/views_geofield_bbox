<?php

/**
 * @file
 * Views plugin definition.
 */

/**
 * Implements hook_views_plugins().
 *
 * Adds GeoJSON feed style.
*/
function views_geofield_bbox_views_plugins() {
  $path = drupal_get_path('module', 'views_geofield_bbox');
  return array(
    'argument default' => array(
      'querystring' => array(
        'title' => t('Bounding box from query string'),
        'handler' => 'views_geofield_plugin_argument_default_bboxquery',
      ),
    ),
  );
}


/**
 * Implements hook_views_handlers().
 *
 * Adds bounding box contextual filter.
 */
function views_geofield_bbox_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_geofield_bbox') . '/views', // path to view files
    ),
    'handlers' => array(
      'views_geofield_bbox_argument' => array(
        'parent' => 'views_handler_argument',
      )
    ),
  );
}

/**
 * Implements hook_views_data().
 *
 * Adds bounding box contextual filter.
 */
function views_geofield_bbox_views_data() {
  $data = array();

  $data['views']['bbox_argument'] = array(
    'group' => t('Mapping'),
    'real field'  => 'bbox_argument',
    'title' => t('Bounding box'),
    'help' => t('Filter locations within a bounding box.'),
    'argument' => array(
      'handler' => 'views_geofield_bbox_argument'
    ),
  );

  return $data;
}
